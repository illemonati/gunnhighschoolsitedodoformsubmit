import React from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      textAlign: 'center',
    },
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'none',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
  }),
);

export default function App() {
  const classes = useStyles();
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [subdomain, setSubdomain] = React.useState('');
  const [error, setError] = React.useState('');
  const [hidden, setHidden] = React.useState(false); 

  const submit = () => {
    const data: object = {
      name: name,
      email: email,
      subdomain: subdomain,
      survey: 'DODO'
    }
    const mail: object = {
      to: email,
      bcc: 'games@tioft.tech',
      subject: 'HELLO DODO',
      template: 'dodo.email'
    }
    fetch('/survey/submit', {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json'
      },
      body : JSON.stringify({survey: data, email: mail})
    }).then();

  }

  const checkAndSubmit = () => {
    let errors = [];
    if (!name) {
      errors.push("Name");
    }
    if (!email) {
      errors.push("Email");
    }
    if (!subdomain) {
      errors.push("Subdomain");
    }
    if (errors.length === 0) {
      setError('');
      submit();
      setHidden(true);
    } else {
      let errorstr = "";
      for (let e of errors) {
        errorstr += `${e}, `;
      }
      errorstr = errorstr.slice(0, -2);
      errorstr += " not entered !";
      setError(errorstr);
    }
  }

  return (
    <div className="App">
      <div hidden={hidden}>
      <h1>Submit a form !</h1> 
      <br />
      <br />
      <form className={classes.container} noValidate autoComplete="off">
        <Container maxWidth="sm">
          <TextField fullWidth required id="name" label="Name" margin="normal" onChange={e => setName(e.target.value)} > </TextField>
          <TextField fullWidth required id="email" label="Email" margin="normal"onChange={e => setEmail(e.target.value)} > </TextField>
          <TextField fullWidth required id="subdomain" label="Subdomain" margin="normal" onChange={e => setSubdomain(e.target.value)} > </TextField>
          <br />
          <br />
          <Button fullWidth variant="outlined" color="primary" className={classes.button} onClick={() => checkAndSubmit()}>
            Submit
          </Button>
          <br />
          <br />
          <h2>{error}</h2>
        </Container>
      </form>
      </div>
      <div hidden={!hidden}>
        <Container maxWidth="sm">
          <h1>Thank you for your Submission!</h1>
          <br />
          <br />
          <Button fullWidth variant="outlined" color="primary" className={classes.button} onClick={() => {setHidden(!hidden)}}>Submit another one</Button>
          </Container>
      </div>
    </div>
  )
}
